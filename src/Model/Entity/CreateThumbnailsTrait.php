<?php
namespace PhpThumb\Model\Entity;

use PhpThumb\Utility\Thumbnailer;

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
trait CreateThumbnailsTrait
{

	/**
	 * Create a resized version of an image.
	 *
	 * @param unknown $originalPath
	 * @param unknown $options
	 * @return string Path to new image.
	 */
	public function createImage($originalPath, $options = []) {
		$thumbnailer = new Thumbnailer();
		return $thumbnailer->create($originalPath, $options);
	}
}