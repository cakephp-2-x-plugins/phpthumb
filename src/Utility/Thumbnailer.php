<?php
namespace PhpThumb\Utility;

use Cake\Core\InstanceConfigTrait;
use Cake\Log\Log;

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class Thumbnailer {
	use InstanceConfigTrait;
	const PHPTHUMB_FORMAT_PNG = 'png';
	const PHPTHUMB_FORMAT_JPG = 'jpeg';
	const PHPTHUMB_FORMAT_GIF = 'gif';

	/**
	 * Default configuration
	 *
	 * Valid formats: 'png', 'jpeg', 'gif'
	 *
	 * @var string[string]
	 */
	public $_defaultConfig = [
			'format' => 'png',
			'display_path' => '/img/phpthumb',
			'error_image' => '/img/error-icon.svg',
			'max_width' => 180,
			'max_height' => 180,
			'allow_enlarging' => false,
			'auto_rotate' => true];

	/**
	 *
	 * @var unknown
	 */
	public $error = false;

	/**
	 *
	 * @var string[]
	 */
	private static $cacheableProperties = [
			'src',
			'new',
			'w',
			'h',
			'wp',
			'hp',
			'wl',
			'hl',
			'ws',
			'hs',
			'f',
			'q',
			'sx',
			'sy',
			'sw',
			'sh',
			'zc',
			'bc',
			'bg',
			'fltr'];

	/**
	 * Instance of phpthumb
	 *
	 * @var phpThumb
	 */
	private $phpThumb;

	/**
	 * Current options being used by phpthumb
	 *
	 * @var string[string]
	 */
	private $options = [];

	/**
	 *
	 * @var unknown
	 */
	private $cacheFilename = false;

	/**
	 * Default constructor.
	 *
	 * @param string[string] $config
	 */
	public function __construct($config = []) {
		if ($config) {
			$this->config($config);
		}
	}

	/**
	 * Initialize a new phpthumb instance.
	 * Also reset error messages.
	 */
	private function initialize() {
		// Reset error code.
		$this->error = false;

		if ($this->phpThumb) {
			return;
		}

		// Create a new instance.
		$this->phpThumb = new \phpthumb();

		// Load config file
		include_once (CONFIG . 'phpThumb.config.php');
		if (!empty($PHPTHUMB_CONFIG)) {
			// Make sure the path exists
			if (!file_exists($PHPTHUMB_CONFIG['cache_directory'])) {
				mkdir($PHPTHUMB_CONFIG['cache_directory'], 0777, true);
			}

			foreach ($PHPTHUMB_CONFIG as $key => $value) {
				$keyname = 'config_' . $key;
				$this->phpThumb->setParameter($keyname, $value);
				if (!preg_match('#(password|mysql)#i', $key)) {
					$this->phpThumb->DebugMessage(
							'setParameter(' . $keyname . ', ' .
									 $this->phpThumb->phpThumbDebugVarDump($value) . ')', __FILE__,
									__LINE__);
				}
			}
		}

		// Set current options to empty;
		$this->options = [];

		// Set options from configuration
		$this->setOptions();

		// Disable debugging
		$this->phpThumb->config_disable_debug = false;
	}

	/**
	 * Mix in settings, but don't override with defaults.
	 *
	 * @param string[string] $options
	 *        	Existing options
	 * @return string[string] Mixed in options
	 */
	private function configToOptions($options = array()) {
		if (!isset($options['f'])) {
			$options['f'] = $this->config('format');
		}
		if (!isset($options['w'])) {
			$options['w'] = $this->config('max_width');
		}
		if (!isset($options['h'])) {
			$options['h'] = $this->config('max_height');
		}
		if (!isset($options['aoe'])) {
			$options['aoe'] = $this->config('allow_enlarging');
		}
		if (!isset($options['ar'])) {
			$options['ar'] = $this->config('auto_rotate');
		}

		return $options;
	}

	/**
	 * Set options for phpThumb instance.
	 *
	 * @param string[string] $options
	 */
	private function setOptions($options = []) {
	    // Update config
	    $this->config($options);
	    
		$options = $this->configToOptions($options);

		foreach ($options as $key => $value) {
			$this->phpThumb->setParameter($key, $value);
			$this->currentOptions[$key] = $value;
		}
	}

	/**
	 *
	 * @param unknown $src
	 * @param unknown $options
	 * @return string
	 */
	private function setFormat($src, $options = array()) {
		$extension = pathinfo($src, PATHINFO_EXTENSION);

		switch (strtolower($extension)) {
			case 'png':
				$options['f'] = self::PHPTHUMB_FORMAT_PNG;
				break;
			case 'jpg':
			case 'jpeg':
				$options['f'] = self::PHPTHUMB_FORMAT_JPG;
				break;
			case 'gif':
				$options['f'] = self::PHPTHUMB_FORMAT_GIF;
				break;
		}

		return $options;
	}

	/**
	 *
	 * @param unknown $options
	 * @return unknown
	 */
	private function getExtension($options = []) {
		if (!empty($options) && !empty($options['f'])) {
			return $options['f'];
		}

		return $this->currentOptions['f'];
	}

	/**
	 *
	 * @todo make this just return the filename and not store it.
	 *
	 * @param unknown $image
	 * @param unknown $options
	 * @return \PhpThumb\Utility\unknown
	 */
	private function setCacheFilename($image, $options = array()) {
		ksort($options);
		$filenameParts = array();

		foreach ($options as $key => $value) {
			if (in_array($key, self::$cacheableProperties)) {
				$filenameParts[$key] = $value;
			}
		}

		$this->cacheFilename = $image;
		foreach ($filenameParts as $key => $value) {
			$this->cacheFilename .= $key . $value;
		}

		$this->cacheFilename = md5($this->cacheFilename) . '.' . $this->getExtension($options);
		return $this->cacheFilename;
	}

	/**
	 *
	 * @param unknown $image
	 * @param unknown $options
	 * @return string filename, relative to WWW_ROOT
	 */
	public function create($image, $options = array()) {
		$this->initialize();

		// $this->phpThumb->config_disable_debug = false;

		$this->setOptions($options);
		$this->setFormat($image);

		$inputFilename = WWW_ROOT . $image;
		$inputFilename = str_replace(DS . DS, DS, $inputFilename);

		$this->phpThumb->config_allow_src_above_docroot = true;
		$this->phpThumb->setSourceFilename($inputFilename);

		if (isset($options['filename'])) {
			$filename = $options['filename'];
		} else {
			$this->setCacheFilename($image, $options);
			$filename = $this->config('display_path') . DS . $this->cacheFilename;
		}

		// Generate the output file name.
		$outputFilename = WWW_ROOT . $filename;
		$outputFilename = str_replace(DS . DS, DS, $outputFilename);

		// Return if the file is already there.
		if (file_exists($outputFilename)) {
			return $filename;
		}

		// Try to create the thumbnail
		if ($this->phpThumb->GenerateThumbnail()) {
			if ($this->phpThumb->RenderToFile($outputFilename)) {
				return $filename;
			}
		}

		// Get any error messages
		$this->error = preg_replace("[^A-Za-z0-9\/: .]", "", $this->phpThumb->fatalerror);
		$this->error = str_replace('phpThumb() v1.7.13-201406261000', '', $this->error);

		Log::write('debug', '[Thumbnailer] Creating thumbnail for: ' . $inputFilename);
		Log::write('debug', '[Thumbnailer] Failed to write image: ' . $outputFilename);
		Log::write('debug', '[Thumbnailer] ' . $this->error);
		return false;
	}
}