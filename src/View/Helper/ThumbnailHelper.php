<?php
namespace PhpThumb\View\Helper;

use Cake\View\Helper;
use PhpThumb\Utility\Thumbnailer;

/**
 * A helper for generating thumbnails automatically.
 *
 * Note: calling config() to set options after calling show() will not affect Thumbnailer currently.
 * This is because of the way we're initializing the Thumbnailer instance.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class ThumbnailHelper extends Helper {
	public $helpers = ['Html'];

	/**
	 * Default configuration
	 *
	 * Valid formats: 'png', 'jpeg', 'gif'
	 *
	 * @var string[string]
	 */
	public $_defaultConfig = [
			'format' => 'png',
			'display_path' => '/img/phpthumb',
			'error_image' => '/img/error-icon.svg',
			'max_width' => 180,
			'max_height' => 180,
			'allow_enlarging' => false,
			'auto_rotate' => true];
	/**
	 *
	 * @var unknown
	 */
	private static $TagOptions = ['alt', 'title', 'class'];
	private $thumbnailer = false;

	/**
	 *
	 * @param unknown $image
	 * @param unknown $options
	 * @return string
	 */
	public function show($image, $options = []) {
		// Make sure Thumbnailer is initialized.
		$this->initialize();

		$tagOptions = $this->getTagOptions();

		// Unset those options before passing them.
		foreach (self::$TagOptions as $entry) {
			unset($options[$entry]);
		}

		$filename = $this->thumbnailer->create($image, $options);

		if ($filename) {
			return $this->Html->image($filename, $tagOptions);
		} else {
			return $this->Html->image(
					$this->config('error_image',
							[
									'alt' => $this->thumbnailer->error,
									'title' => $this->thumbnailer->error]));
		}
	}

	/**
	 * Initialize a new Thumbnailer instance.
	 */
	private function initialize() {
		if (!$this->thumbnailer) {
			$this->thumbnailer = new Thumbnailer($this->config());
		}
	}

	/**
	 *
	 * @param unknown $options
	 * @return unknown
	 */
	private function getTagOptions() {
	    $options = [];
	    
		foreach (self::$TagOptions as $key) {
			if (isset($this->currentOptions[$key])) {
				$options[$key] = $this->currentOptions[$key];
			}
		}

		return $options;
	}
}